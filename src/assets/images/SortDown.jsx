import React from "react";

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="15px"
      height="15px"
      version="1.1"
      viewBox="0 -256 1792 1792"
    >
      <g transform="matrix(1 0 0 -1 364.475 867.797)">
        <path
          fill="#ffffff"
          d="M1024 448q0-26-19-45L557-45q-19-19-45-19t-45 19L19 403Q0 422 0 448t19 45q19 19 45 19h896q26 0 45-19t19-45z"
        ></path>
      </g>
    </svg>
  );
}

export default Icon;