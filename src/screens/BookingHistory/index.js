import React, { Component } from 'react';
import { connect } from 'react-redux';

import './BookingHistory.css';

class BookingHistory extends Component {

    render() {
        return (
            <div className='w-100 booking-history'>
                <div className='m-auto booking-history-container'>
                    <h4>Recent Bookings</h4>
                    <div className='w-100 d-flex flex-column bookings-container'>
                        {
                            this.props.orders.map((order) => {
                                return (
                                    <div key={order.id} className='w-100 order'>
                                        <div className='d-flex flex-column  align-items-start'>
                                            <h5>{order.title}</h5>
                                            <p>{order.language}</p>
                                            <h5>{order.date} | {order.time}</h5>
                                            <p>{order.cinema}</p>
                                            <h6>{order.seats.length} ticket :- {order.seats.join(', ')}</h6>
                                        </div>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const orders = state.orders.list;

    return {
        orders: orders ? orders : []
    }
}

export default connect(mapStateToProps)(BookingHistory);