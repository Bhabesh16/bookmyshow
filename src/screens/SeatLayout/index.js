import React, { Component } from 'react';
import { connect } from 'react-redux';

import './SeatLayout.css';
import angelLeft from "../../assets/images/angel-left.png";
import screen from '../../assets/images/screen.png';
import BookingSummary from '../../components/BookingSummary';
import { Link } from 'react-router-dom';

class SeatLayout extends Component {

    constructor(props) {
        super(props);

        this.state = {
            movieTitle: "",
            language: "",
            cinema: {},
            time: "",
            seats: [],
            select: false,
            cost: 0,
            selectedSeats: [],
            bookingPage: false
        }
    }

    selectSeat = (occupied, seatIndex, arrayIndex, seatNumber) => {
        const seatValue = String.fromCharCode(arrayIndex + 'A'.charCodeAt(0)) + seatNumber;
        if (occupied) {
            return
        } else {
            this.setState((prevState) => {
                const updatedSeats = [
                    ...prevState.seats
                ];
                updatedSeats[seatIndex] = !updatedSeats[seatIndex];
                const addCost = updatedSeats[seatIndex] ? 150 : -150;

                let newSeats;
                if (!updatedSeats[seatIndex]) {
                    newSeats = prevState.selectedSeats.filter((seat) => {
                        return seat !== seatValue;
                    })
                } else {
                    newSeats = [
                        ...prevState.selectedSeats,
                        seatValue
                    ];
                }

                return {
                    ...prevState,
                    seats: updatedSeats,
                    cost: prevState.cost + addCost,
                    selectedSeats: newSeats
                }
            });
        }
    }

    handleClick = () => {
        this.setState({
            bookingPage: true
        });

    }

    componentDidMount() {
        let [movieTitle, language, screenId, time] = window.location.href.split('/').slice(4);
        movieTitle = movieTitle.replaceAll('-', " ");
        time = time.replaceAll("-", " ");

        const cinema = this.props.cinemas.find((cinema) => {
            return cinema.id === screenId;
        });

        const seats = cinema.seats[time];

        this.setState({
            movieTitle,
            language,
            cinema,
            time,
            seats
        });
    }

    goBack = () => {
        setTimeout(() => {
            window.location.reload();
        }, 10);
    }

    render() {
        const movieTitle = this.state.movieTitle[0]?.toUpperCase() + this.state.movieTitle.slice(1);
        return (
            <div className='w-100 seat-layout'>
                <div className='w-100 px-4 d-flex justify-content-between seat-layout-nav text-white'>
                    <div className='d-flex align-items-center seat-layout-left'>
                        <div className='image-wrap'>
                            <Link to={-1} onClick={this.goBack}>
                                <img src={angelLeft} alt="" width="100%" />
                            </Link>
                        </div>
                        <div>
                            <h6>{movieTitle}</h6>
                            <p>{this.state.cinema?.title} | {this.state.time}</p>
                        </div>
                    </div>
                    <h6>X</h6>
                </div>
                {!this.state.bookingPage &&
                    <>
                        <div className='w-100 d-flex flex-column align-items-center seat-layout-container'>
                            {Array(10).fill(0).map((value, arrayIndex) => {
                                return (
                                    <div className='d-flex' key={value + arrayIndex}>
                                        {
                                            this.state.seats.slice((arrayIndex * 10), (arrayIndex + 1) * 10).map((seat, seatIndex) => {
                                                let letter = '';
                                                if (seatIndex === 0) {
                                                    letter = String.fromCharCode(arrayIndex + 'A'.charCodeAt(0))
                                                }
                                                const calSeatIndex = arrayIndex * 10 + seatIndex;
                                                const occupied = this.state.cinema.seats[this.state.time][calSeatIndex];

                                                return (
                                                    <div className='d-flex text-success' key={String(seat) + calSeatIndex}>
                                                        {seatIndex === 0 &&
                                                            <div className='seat-letter text-dark'>
                                                                {letter}
                                                            </div>
                                                        }
                                                        {(seatIndex === 3 || seatIndex === 7) &&
                                                            <div className='px-5'></div>

                                                        }
                                                        <div className={occupied ? "occupied-seat seat-container" : seat ? "selected-seat seat-container" : "seat-container"} onClick={() => {
                                                            this.selectSeat(occupied, calSeatIndex, arrayIndex, seatIndex + 1)
                                                        }}>
                                                            {seatIndex + 1}
                                                        </div>
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                                );
                            })
                            }
                        </div>
                        <div className='d-flex flex-column align-items-center w-100 movie-screen-container'>
                            <img src={screen} alt="" width="500rem" />
                            <p>All eyes this way please!</p>
                        </div>
                        <div className='d-flex justify-content-center seat-layout-bottom-container'>
                            <div><span className='box1'></span> Sold</div>
                            <div><span className='box2'></span> Available</div>
                            <div><span className='box3'></span> Selected</div>
                        </div>
                        {this.state.cost !== 0 &&
                            <div className='d-flex justify-content-center my-5 pay-button-container'>
                                <span className='d-flex justify-content-center pay-button align-items-center' onClick={this.handleClick}>Pay Rs. {this.state.cost}</span>
                            </div>
                        }
                    </>
                }
                {
                    this.state.bookingPage && <BookingSummary bookingState={this.state} />
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cinemas: state.movies.cinemas
    }
}

export default connect(mapStateToProps)(SeatLayout);