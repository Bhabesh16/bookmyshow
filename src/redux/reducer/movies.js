import { CINEMAS, ERROR, MOVIES, UPDATE_CINEMA } from "../actions/moviesAction";

const initialState = {
    list: null,
    cinemas: null,
    error: ""
}

export default function actionMovies(state = initialState, action) {
    switch (action.type) {
        case ERROR: {
            return {
                ...state,
                error: action.payload
            }
        }

        case MOVIES: {
            return {
                ...state,
                list: action.payload
            }
        }

        case CINEMAS: {
            return {
                ...state,
                cinemas: action.payload
            }
        }

        case UPDATE_CINEMA: {
            return {
                ...state,
                cinemas: state.cinemas.map((cinema) => {
                    return cinema.id === action.payload.id ? action.payload : cinema;
                })
            }
        }

        default: {
            return state;
        }
    }
}