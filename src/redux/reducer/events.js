import allEvents from '../../assets/jsonData/allEvents.json';
import footerArticles from '../../assets/jsonData/footerArticles.json';
import liveEvents from '../../assets/jsonData/liveEvents.json';
import premieresMovies from '../../assets/jsonData/premieresMovies.json';
import movies from '../../assets/jsonData/RecommendedMovies.json';
import allMoviesAndEvents from '../../assets/jsonData/allMoviesAndEvents.json';

const initialState = {
    list: allEvents,
    movies: movies,
    liveEvents: liveEvents,
    premieresMovies: premieresMovies,
    footerArticles: footerArticles,
    allMoviesAndEvents: allMoviesAndEvents
}

export default function actionProduct(state = initialState, action) {
    switch (action.type) {

        default: {
            return state;
        }
    }
}