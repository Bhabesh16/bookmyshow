import axios from "axios";

import { showErrors } from "./moviesAction";

export const ERROR = 'ERROR';
export const ADD_CURRENT_USER = "ADD_CURRENT_USER";
export const REMOVE_CURRENT_USER = "REMOVE_CURRENT_USER";

export const addCurrentUser = (user) => ({
    type: ADD_CURRENT_USER,
    payload: user
});

export const removeCurrentUser = () => ({
    type: REMOVE_CURRENT_USER
});

export const createUser = (userData) => (dispatch) => {
    console.log("createuser", userData)
    axios.post('https://bookmyshowapi.vercel.app/api/users', userData)
        .then((response) => {
            dispatch(addCurrentUser(response.data));
            localStorage.setItem('currentUser', JSON.stringify(response.data));
        })
        .catch((err) => {
            console.error(err);
            const errorMessage = "Cannot create user, " + err.message;
            dispatch(showErrors(errorMessage))
        });
}

export const addUsers = (userData) => (dispatch) => {
    axios.get('https://bookmyshowapi.vercel.app/api/users')
        .then((response) => {
            const users = response.data;
            const getUser = users.find((user) => {
                return user.mobileNumber === Number(userData.mobileNumber);
            });

            if (getUser === undefined) {
                dispatch(createUser(userData));
            } else {
                dispatch(addCurrentUser(getUser));
                localStorage.setItem('currentUser', JSON.stringify(getUser));
            }

        })
        .catch((err) => {
            console.error(err);
            const errorMessage = "Cannot Fetch Users from API, " + err.message;
            dispatch(showErrors(errorMessage))
        });
}

export const removeUser = () => (dispatch) => {
    dispatch(removeCurrentUser());
    localStorage.removeItem('currentUser');
}

export const getCurrentUser = () => (dispatch) => {
    let currentUser = localStorage.getItem('currentUser');
    currentUser = currentUser ? JSON.parse(currentUser) : null;

    dispatch(addCurrentUser(currentUser));
}