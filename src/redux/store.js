import { configureStore } from '@reduxjs/toolkit';

import movies from './reducer/movies';
import events from './reducer/events';
import users from './reducer/users';
import orders from './reducer/orders';

const reducer = {
    movies,
    events,
    users,
    orders
};

export default configureStore({
    reducer
});
