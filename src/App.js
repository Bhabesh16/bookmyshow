import React, { Component } from 'react';
import { Route, Routes } from 'react-router-dom';
import { connect } from 'react-redux';

import './App.css';
import Navbar from './components/Navbar';
import AdvertiseCarousel from './components/AdvertiseCarousel';
import RecommendedMovies from './components/RecommendedMovies';
import BookMyShowStream from './components/BookMyShowStream';
import LiveEvents from './components/LiveEvents';
import Premiere from './components/Premiere';
import PremieresMovies from './components/PremieresMovies';
import AllEvents from './components/AllEvents';
import Footer from './components/Footer';
import HomeText from './components/HomeText';
import MovieScreen from './screens/MovieScreen';
import EventScreen from './screens/EventScreen';
import SportScreen from './screens/SportScreen';
import AllSearch from './screens/AllSearch';
import { fetchCinemas, fetchMovies } from './redux/actions/moviesAction';
import BuyMovieTicket from './screens/BuyMovieTicket';
import Errors from './components/layouts/Errors';
import Loader from './components/layouts/Loader';
import SeatLayout from './screens/SeatLayout';
import { getCurrentUser } from './redux/actions/userAction';
import { fetchOrders } from './redux/actions/ordersAction';
import BookingHistory from './screens/BookingHistory';

class App extends Component {

  componentDidMount() {
    this.props.fetchMovies();
    this.props.fetchCinemas();
    this.props.getCurrentUser();
  }

  componentDidUpdate() {
    if (!this.props.orders) {
      this.props.fetchOrders(this.props.currentUser);
    } else {
      return;
    }
  }

  render() {
    const { error, movies, cinemas } = this.props;

    return (
      <div className='App'>
        <Navbar />
        {!error && (!movies || !cinemas) && <Loader />}
        {error && <Errors errorMessage={error} />}
        {!error && movies?.length > 0 && cinemas?.length > 0 &&
          <Routes>
            <Route path="/" element={
              <>
                <AdvertiseCarousel />
                <RecommendedMovies />
                <BookMyShowStream />
                <LiveEvents />
                <Premiere />
                <PremieresMovies />
                <AllEvents />
                <HomeText />
              </>
            } />
            <Route path="/:name" element={<AllSearch />} />
            <Route path="/movies/:name" element={<MovieScreen />} />
            <Route path="/events/:name" element={<EventScreen />} />
            <Route path="/sports/:name" element={<SportScreen />} />
            <Route path='/myprofile/booking-history' element={<BookingHistory />} />
            <Route path="/buytickets/:movie/:language" element={<BuyMovieTicket />} />
            <Route path="/buytickets/:movie/:language/:id/:time" element={<SeatLayout />} />
          </Routes>
        }
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.movies.error,
    movies: state.movies.list,
    cinemas: state.movies.cinemas,
    currentUser: state.users.currentUser,
    orders: state.orders.list
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMovies: () => {
      dispatch(fetchMovies())
    },
    fetchCinemas: () => {
      dispatch(fetchCinemas())
    },
    getCurrentUser: () => {
      dispatch(getCurrentUser())
    },
    fetchOrders: (currentUser) => {
      dispatch(fetchOrders(currentUser))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);