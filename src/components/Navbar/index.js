import React, { Component } from 'react';
import NavBottom from '../layouts/NavBottom';
import NavTop from '../layouts/NavTop';

class Navbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showNav: true
    };
  }

  componentDidMount() {
    const url = window.location.href.split('/');
    if (url.length === 8) {
      this.setState({
        showNav: false
      })
    } else {
      this.setState({
        showNav: true
      })
    }
  }

  render() {
    return (
      <div>
        {this.state.showNav &&
          <>
            <NavTop />
            <NavBottom />
          </>
        }
      </div>
    );
  }
}

export default Navbar;