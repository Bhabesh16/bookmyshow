import React, { Component } from 'react';

import './BookMyShowStream.css';

export class BookMyShowStream extends Component {
    render() {
        return (
            <div className='stream-image'>
                <img
                    src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-1440,h-120:q-80/stream-leadin-web-collection-202210241242.png"
                    alt=""
                    width='100%'
                />
            </div>
        );
    }
}

export default BookMyShowStream;