import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './BookingSummary.css';
import bookASmile from '../../assets/images/book-a-smile.png';
import { addNewOrder } from '../../redux/actions/ordersAction';
import { updateCinemaToApi } from '../../redux/actions/moviesAction';

class BookingSummary extends Component {

    constructor(props) {
        super(props);

        this.state = {
            contribution: 2,
            successMessage: false
        };
    }

    onClickContribution = () => {
        if (this.state.contribution === 0) {
            this.setState({
                contribution: 2
            });
        } else {
            this.setState({
                contribution: 0
            });
        }
    }

    getTodaysDate = () => {
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let today = new Date().toISOString().slice(0, 10);
        today = today.split("-");
        const date = today[2] + " " + months[Number(today[1])] + " " + today[0];
        return date;
    }

    bookTicket = () => {
        const date = this.getTodaysDate();

        const order = {
            userId: this.props.currentUser.id,
            title: this.props.bookingState.movieTitle,
            language: this.props.bookingState.language,
            date: date,
            time: this.props.bookingState.time,
            cinema: this.props.bookingState.cinema.title,
            seats: this.props.bookingState.selectedSeats
        }

        this.setState({
            successMessage: true
        });

        const newCinema = {
            id: this.props.bookingState.cinema.id,
            seats: {
                ...this.props.bookingState.cinema.seats,
                [this.props.bookingState.time]: this.props.bookingState.seats
            }
        }

        this.props.updateCinemaToApi(newCinema);
        this.props.addNewOrder(order);
    }

    homeButton = () => {
        setTimeout(() => {
            window.location.reload();
        }, 10);
    }

    render() {
        const selectedSeats = this.props.bookingState.selectedSeats;
        const cost = selectedSeats.length * 150;
        const convinienceFee = cost / 6.25;
        const totalCost = (cost + convinienceFee + this.state.contribution);

        const successMessage = this.state.successMessage;

        return (
            <div className='d-flex flex-column align-items-center justify-content-center w-100 booking-summary'>
                <div className='bg-white booking-container'>
                    <h3 className='title'>{successMessage ? "Ticket Details" : "BOOKING SUMMARY"}</h3>
                    <div className='py-3 d-flex justify-content-between tickets-container'>
                        <span className='d-flex align-items-center text-dark seat'>
                            Seats - {selectedSeats.join(', ')}
                            <p> ({selectedSeats?.length} Tickets)</p>
                        </span>
                        <h5>Rs. {cost.toFixed(2)}</h5>

                    </div>
                    <div className='w-100 d-flex justify-content-between'>
                        <p>Convinience fees</p>
                        <h5>Rs. {convinienceFee.toFixed(2)}</h5>
                    </div>
                    <div className='ticket-container-border'></div>

                    <div className='w-100 d-flex justify-content-between'>
                        <span className='m-0'>Sub total</span>
                        <h5>Rs. {cost + convinienceFee}</h5>
                    </div>
                    <span className='circle-left'></span>
                    <span className='circle-right'></span>
                    <div className='w-100 p-4 my-5 d-flex align-items-start justify-content-between contribution-container'>
                        {!successMessage && this.state.contribution === 2 &&
                            <>
                                <div className='left-contribution-container'>
                                    <img src={bookASmile} alt="" width="100%" />
                                </div>
                                <div className='mid-contribution-container'>
                                    <h6>Contribution to BookASmile</h6>
                                    <p>(₹1 per ticket has been added)</p>
                                    <span>VIEW T&C</span>
                                </div>
                                <div className='right-contribution-container'>
                                    <h5>Rs. 2</h5>
                                    <p onClick={this.onClickContribution}>Remove</p>
                                </div>
                            </>
                        }
                        {!successMessage && this.state.contribution === 0 &&
                            <>
                                <div className='left-contribution-container'>
                                    <img src={bookASmile} alt="" width="100%" />
                                </div>
                                <div className='mid-contribution-container'>
                                    <h6>Contribute to BookASmile</h6>
                                    <p>(₹1 per ticket will be added)</p>
                                    <span>VIEW T&C</span>
                                </div>
                                <div className='right-contribution-container'>
                                    <h5>+ Rs. 2</h5>
                                    <p onClick={this.onClickContribution}>Add</p>
                                </div>
                            </>
                        }
                        {successMessage &&
                            <div className='w-100 d-flex justify-content-center'>
                                <img
                                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/1200px-QR_code_for_mobile_English_Wikipedia.svg.png"
                                    alt=""
                                    width="50%"
                                />
                            </div>
                        }
                    </div>

                    <div className="w-100 d-flex justify-content-between bottom-payable">
                        <span>{successMessage ? "Total Amount Paid" : "Amount Payable"}</span>
                        <h5>Rs. {totalCost.toFixed(2)}</h5>
                    </div>
                </div>
                {!successMessage &&
                    <>
                        <div className='d-flex align-items-center justify-content-left bottom-payment-container'>
                            <span className='info'>i</span>
                            <span>By proceeding, I express my consent to complete this transaction.</span>
                        </div>
                        <div className='d-flex align-items-center justify-content-between payment-button' onClick={this.bookTicket}>
                            <span>Total: Rs. {totalCost.toFixed(2)}</span>
                            <span>Proceed</span>
                        </div>
                    </>
                }
                {successMessage &&
                    <Link to="/">
                        <button className='home-button' onClick={this.homeButton}>Back to Home</button>
                    </Link>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.users.currentUser
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addNewOrder: (order) => {
            dispatch(addNewOrder(order))
        },
        updateCinemaToApi: (cinema) => {
            dispatch(updateCinemaToApi(cinema))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingSummary);