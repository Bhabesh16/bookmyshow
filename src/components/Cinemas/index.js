import React, { Component } from 'react';

import './Cinemas.css';
import Heart from '../../assets/images/Heart';
import TermsAndCondition from '../TermsAndCondition';

class Cinemas extends Component {

    render() {
        const allTimes = this.props.cinema.moviesTime[this.props.movieTitle];

        return (
            <div className='d-flex w-100 cinemas'>
                <div className='heart-icon'><Heart /></div>
                <h6>{this.props.cinema?.title}</h6>
                <div className='cinemas-info'><span>i</span> INFO</div>
                <div className='d-flex movie-time-container'>
                    {
                        allTimes?.map((time, index) => {
                            return (
                                <div key={time} className="time-container" data-toggle="modal" data-target={"#termsCondition" + index + time}>
                                    {time}
                                    <TermsAndCondition cinema={this.props.cinema} time={time} handleUrl={this.handleUrl} index={index} />
                                </div>
                            );
                        })
                    }
                </div>

            </div>
        );
    }
}

export default Cinemas;