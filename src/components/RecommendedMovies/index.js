import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import './RecommendedMovies.css';

class RecommendedMovies extends Component {

  render() {
    return (
      <div className='carousel carousel-style'>
        <div className='d-flex justify-content-between align-items-center my-3'>
          <h4 className='head'>Recommended Movies</h4>
          <Link to="/movies" className='see-all'>{'See All >'}</Link>
        </div>
        {/* <div className='d-flex w-100 align-items-center py-3'> */}
        <div id="carouselExampleControls" className="carousel slide w-100" data-wrap="false" data-interval="false">
          <div className="carousel-inner d-flex">
            <div className="carousel-item active">
              <div className='d-flex movies-contain'>
                {
                  this.props.movies?.slice(0, 5)
                    .map((movie) => {
                      const movieUrl = "/movies/" + movie.title.toLowerCase()
                        .replaceAll('-', '')
                        .replaceAll('  ', ' ')
                        .replaceAll(' ', '-');

                      return (
                        <Link to={movieUrl} key={movie.id}>
                          <div className="card movie-card" style={{ width: "14em", height: "30em" }}>
                            <img className="card-img-top" src={movie.image} alt="" style={{ width: '100%', height: '75%' }} />
                            <h6 className="card-title">{movie.title}</h6>
                            <p>{movie.type}</p>
                          </div>
                        </Link>
                      )
                    })
                }
              </div>
            </div>
            <div className="carousel-item">
              <div className='d-flex movies-contain'>
                {
                  this.props.movies?.slice(5, 10)
                    .map((movie) => {
                      const movieUrl = "/movies/" + movie.title.toLowerCase()
                        .replaceAll('-', '')
                        .replaceAll('  ', ' ')
                        .replaceAll(' ', '-');

                      return (
                        <Link to={movieUrl} key={movie.id}>
                          <div className="card movie-card" style={{ width: "14em", height: "30em" }}>
                            <img className="card-img-top" src={movie.image} alt="" style={{ width: '100%', height: '75%' }} />
                            <h6 className="card-title">{movie.title}</h6>
                            <p>{movie.type}</p>
                          </div>
                        </Link>
                      )
                    })
                }
              </div>
            </div>
          </div>
          <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon arrowTop" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span className="carousel-control-next-icon arrowTop" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>

      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movies.list
  }
}

export default connect(mapStateToProps)(RecommendedMovies);