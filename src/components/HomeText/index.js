import React, { Component } from 'react';

import './HomeText.css';

class HomeText extends Component {
    render() {
        return (
            <div className='w-100 bg-white'>
                <div className='home-text'>
                    <span>Home</span>
                </div>
            </div>
        );
    }
}

export default HomeText;