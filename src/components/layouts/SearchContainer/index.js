import React, { Component } from 'react';

import './SearchContainer.css';
import Movie from '../../../assets/images/Movie';
import LiveEvents from '../../../assets/images/LiveEvents';
import Sports from '../../../assets/images/Sports';
import { Link } from 'react-router-dom';


class SearchContainer extends Component {

    handleClick = () => {
        this.props.clearInput();
    }

    render() {
        return (
            <div className='search-container'>
                {this.props.searchEvents.length === 0 &&
                    <div className="d-flex search-text-box">
                        <span style={{ color: "#F84464" }}>No Results Found</span>
                    </div>
                }

                {this.props.searchEvents.length !== 0 &&
                    this.props.searchEvents.map((event) => {
                        let linkUrl = '/';
                        if (event.topic === "Movies") {
                            linkUrl += 'movies/';
                        } else if (event.topic === "Sports") {
                            linkUrl += 'sports/';
                        } else {
                            linkUrl += 'events/';
                        }
                        linkUrl += event.title.toLowerCase()
                            .replaceAll('-', '')
                            .replaceAll('  ', ' ')
                            .replaceAll(' ', '-');

                        return (
                            <Link to={linkUrl} key={event.id} onClick={this.handleClick}>
                                <div className="d-flex search-text-box">
                                    {event.topic === "Movies" && <Movie />}
                                    {event.topic === "Sports" && <Sports />}
                                    {event.topic.includes('Events') && <LiveEvents />}
                                    <span style={{ color: "#666666" }}>{event.title}</span>
                                </div>
                            </Link>
                        );
                    })
                }
            </div>
        );
    }
}

export default SearchContainer;