import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './NavTop.css';
import logo from '../../../assets/images/logo.svg';
import magnifier from '../../../assets/images/magnifier.png';
import SortDown from '../../../assets/images/SortDown.jsx';
import menuBar from '../../../assets/images/menu-bar.png';
import Signin from '../../Authentication/Signin';
import { connect } from 'react-redux';
import SearchContainer from '../SearchContainer';
import filterEvents from '../../../utils/filterEvents';
import UserSidebar from '../UserSidebar';

class NavTop extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isSearch: false,
      searchEvents: {},
      sideBar: false
    };
  }

  clearInput = () => {
    this.setState({
      isSearch: false
    });
  }

  handleChange = (event) => {
    event.preventDefault();

    const search = event.target.value;

    if (search.length > 0) {

      const data = filterEvents(this.props.events, search);
      this.setState({
        isSearch: true,
        searchEvents: data
      })
    } else {
      this.setState({
        isSearch: false
      });
    }
  }

  handleSideBar = () => {
    this.setState({
      sideBar: !this.state.sideBar
    });
  }

  render() {
    return (
      <>
        <div className='w-100 nav-top'>
          <div className='w-92 h-100 d-flex justify-content-between align-items-center nav-top-contain'>
            <div className='d-flex justify-content-start align-items-center h-100 w-75'>
              <div className='logo'>
                <Link to="/" className='d-flex align-items-center justify-content-center nav-top-left-contain'>
                  <img src={logo} alt="" />
                </Link>
              </div>
              <div className='w-100'>
                <div className='d-flex w-100 justify-content-start bg-white align-items-center search-bar'>
                  <img src={magnifier} alt="" className='magnifier' />
                  <input type="text" onChange={this.handleChange} placeholder='Search for Movies, Events, Plays, Sports and Activities' className='w-100' />

                  {this.state.isSearch && <SearchContainer searchEvents={this.state.searchEvents} clearInput={this.clearInput} />}
                </div>
              </div>
            </div>
            <div className='d-flex justify-content-end align-items-center h-100'>
              <div className='d-flex justify-content-center align-items-center city'>
                <span>Bengaluru</span>
                <SortDown />
                {Object.keys(this.props.user).length === 0 &&
                  <>
                    <button className='signin-button' data-toggle="modal" data-target="#exampleModal">
                      Sign in
                    </button>
                    <Signin />
                    <img src={menuBar} alt="" onClick={this.handleSideBar} />
                    {this.state.sideBar && <UserSidebar isUser={false} />}
                  </>
                }
                {Object.keys(this.props.user).length !== 0 &&
                  <div onClick={this.handleSideBar}>
                    <img className='mx-1' src="https://in.bmscdn.com/m6/images/my-profile/bms-user.png" alt="" />
                    <span>Hi, {this.props.user.name}</span>
                    {this.state.sideBar && <UserSidebar isUser={true} />}
                  </div>
                }
              </div>

            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const user = state.users.currentUser;
  return {
    user: user ? user : {},
    events: state.events.allMoviesAndEvents,
  }
}

export default connect(mapStateToProps)(NavTop);