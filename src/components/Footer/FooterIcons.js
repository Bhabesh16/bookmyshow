import React, { Component } from 'react';

import './Footer.css';
import CustomerSupport from '../../assets/images/CustomerSupport';
import BookingConfirm from '../../assets/images/BookingConfirm';
import Email from '../../assets/images/Email';

class FooterIcons extends Component {
    render() {
        return (
            <div className="w-100 footer-icons">
                <div className="d-flex justify-content-around footer-list-contain p-0">
                    <div className='d-flex flex-column justify-content-center align-items-center footer-icon-wrap'>
                        <CustomerSupport />
                        <h6>24/7 CUSTOMER CARE</h6>
                    </div>
                    <div className='d-flex flex-column justify-content-center align-items-center footer-icon-wrap'>
                        <BookingConfirm />
                        <h6>RESEND BOOKING CONFIRMATION</h6>
                    </div>
                    <div className='d-flex flex-column justify-content-center align-items-center footer-icon-wrap'>
                        <Email />
                        <h6>SUBSCRIBE TO THE NEWSLETTER</h6>
                    </div>
                </div>
            </div>
        )
    }
}

export default FooterIcons;