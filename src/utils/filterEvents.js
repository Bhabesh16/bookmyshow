

const filterEvents = (events, searchText) => {
    searchText = searchText.toLowerCase();

    const newList = events.filter((event) => {
        if (event.title.toLowerCase().includes(searchText)) {
            return true;
        } else if (event.description && event.description.toLowerCase().includes(searchText)) {
            return true;
        } else {
            return false;
        }
    });
    return newList.slice(0, 8);

}

export default filterEvents;